package me.daniel.LearEnglishWords.models;

import javax.validation.constraints.NotNull;

public class Question {
	@NotNull
	private String polish;

	@NotNull
	private String english;

	/*
	 * Getters and setters
	 */
	
	public String getPolish() {
		return polish;
	}

	public void setPolish(String polish) {
		this.polish = polish;
	}

	public String getEnglish() {
		return english;
	}

	public void setEnglish(String english) {
		this.english = english;
	}
	
	
}
