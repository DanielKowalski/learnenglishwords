package me.daniel.LearEnglishWords.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
public class Translation {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@NotNull
	private String polish;
	
	@NotNull
	private String english;
	
	@NotNull
	@Min(3)
	@Max(4)
	private Integer year; 
	
	@NotNull
	@Min(1)
	@Max(8)
	private Integer chapter;

	@Override
	public String toString() {
		return "Translation [id=" + id + ", polish=" + polish + ", english=" + english + ", year=" + year + ", chapter="
				+ chapter + "]";
	}
	
	/*
	 * Getters and setters
	 */

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPolish() {
		return polish;
	}

	public void setPolish(String polish) {
		this.polish = polish;
	}

	public String getEnglish() {
		return english;
	}

	public void setEnglish(String english) {
		this.english = english;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getChapter() {
		return chapter;
	}

	public void setChapter(Integer chapter) {
		this.chapter = chapter;
	}
}
