package me.daniel.LearEnglishWords;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearEnglishWordsApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearEnglishWordsApplication.class, args);
	}
}
