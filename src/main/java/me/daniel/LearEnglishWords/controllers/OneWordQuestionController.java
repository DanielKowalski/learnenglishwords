package me.daniel.LearEnglishWords.controllers;

import java.util.ArrayList;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import me.daniel.LearEnglishWords.models.Question;
import me.daniel.LearEnglishWords.models.Translation;
import me.daniel.LearEnglishWords.repositories.TranslationRepository;

@Controller
public class OneWordQuestionController {
	@Autowired
	private TranslationRepository repo;
	
	@GetMapping("/q")
	public String question(Question question) {
		ArrayList<Translation> translations = repo.findAll();
		Integer index = new Random().nextInt(translations.size());
		String polish = translations.get(index).getPolish();
		question.setPolish(polish);
		return "question";
	}
	
	@GetMapping(path="/q", params={"year", "chapter"})
	public String question(Question question, @RequestParam("year") Integer year, @RequestParam("chapter") Integer chapter,
			Model model) {
		if (chapter == null || year == null) {
			return "redirect:/q";
		}
		ArrayList<Translation> translations = repo.findByYearAndChapter(year, chapter);
		Integer index = new Random().nextInt(translations.size());
		String polish = translations.get(index).getPolish();
		question.setPolish(polish);
		model.addAttribute("year", year);
		model.addAttribute("chapter", chapter);
		return "question";
	}
	
	@PostMapping("/q")
	public String checkAnswer(Question question, Model model, @RequestParam(value="year",required=false) Integer year,
			@RequestParam(value="chapter",required=false) Integer chapter, RedirectAttributes redirectAttributes) {
		Translation word = repo.findByPolish(question.getPolish());
		String myAnswer = question.getEnglish().toLowerCase().trim();
		String correctAnswer = word.getEnglish().toLowerCase().trim();
		model.addAttribute("ok", myAnswer.equals(correctAnswer) ? "Dobrze" : "Źle");
		model.addAttribute("correct", correctAnswer);
		model.addAttribute("your", myAnswer);
		model.addAttribute("polish", word.getPolish());
		model.addAttribute("year", year);
		model.addAttribute("chapter", chapter);
		return "result";
	}
}
