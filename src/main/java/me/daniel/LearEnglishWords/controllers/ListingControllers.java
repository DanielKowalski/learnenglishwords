package me.daniel.LearEnglishWords.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import me.daniel.LearEnglishWords.models.Translation;
import me.daniel.LearEnglishWords.repositories.TranslationRepository;

@Controller
public class ListingControllers {
	@Autowired
	private TranslationRepository repo;
	
	@GetMapping("/list")
	public @ResponseBody String getWordsList() {
		String result = "<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>";
		result = result.concat("<body><h3>Wszystkie słówka w bazie:</h3>");
		for (Translation word : repo.findAll()) {
			result = result.concat("<b>" + word.getEnglish() + "</b>");
			result = result.concat(" ");
			result = result.concat(word.getPolish());
			result = result.concat("<br />");
		}
		result = result.concat("</body>");
		return result;
	}
	
	@GetMapping("/alphabeticalList")
	public @ResponseBody String getWordsListInAlphabeticalOrder() {
		String result = "<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>";
		result = result.concat("<body><h3>Wszystkie słówka posegregowane alfabetycznie:</h3>");
		for (Translation word : repo.findAllByOrderByEnglishAsc()) {
			result = result.concat("<b>" + word.getEnglish() + "</b>");
			result = result.concat(" ");
			result = result.concat(word.getPolish());
			result = result.concat("<br />");
		}
		result = result.concat("</body>");
		return result;
	}
}
