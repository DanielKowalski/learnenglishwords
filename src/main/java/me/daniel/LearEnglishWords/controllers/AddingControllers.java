package me.daniel.LearEnglishWords.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import me.daniel.LearEnglishWords.models.Translation;
import me.daniel.LearEnglishWords.repositories.TranslationRepository;

@Controller
public class AddingControllers {
	@Autowired
	private TranslationRepository repo;
	
	@GetMapping("/add")
	public String add(Translation translation) {
		return "add";
	}
	
	@PostMapping("/add")
	public String checkNewTranslation(@Valid Translation translation, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "add";
		}
		repo.save(translation);
		return "add";
	}
}
