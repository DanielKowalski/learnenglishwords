package me.daniel.LearEnglishWords.controllers;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WebController implements ErrorController {
	
	@GetMapping("/")
	public String index() {
		return "index";
	}
	
	@GetMapping("/error") 
	public String error(HttpServletRequest request){
		Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
		if (status != null) {
			Integer statusCode = Integer.valueOf(status.toString());
			if (statusCode == HttpStatus.NOT_FOUND.value()) {
				return "redirect:/";
			}
		}
		return "error";
	}
	
	@Override
	public String getErrorPath() {
		return "/error";
	}
}
