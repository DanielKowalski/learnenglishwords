package me.daniel.LearEnglishWords.repositories;

import java.io.Serializable;
import java.util.ArrayList;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

@NoRepositoryBean
public interface MyBaseRepository<T, ID extends Serializable> extends Repository<T, ID>{
	T save(T entity);
	ArrayList<T> findAll();
}
