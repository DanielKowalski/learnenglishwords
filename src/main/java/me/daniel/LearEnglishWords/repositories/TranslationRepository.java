package me.daniel.LearEnglishWords.repositories;

import java.util.ArrayList;

import me.daniel.LearEnglishWords.models.Translation;

public interface TranslationRepository extends MyBaseRepository<Translation, Integer> {
	Translation findByPolish(String polish);
	ArrayList<Translation> findByYearAndChapter(Integer year, Integer chapter);
	ArrayList<Translation> findAllByOrderByEnglishAsc();
}
